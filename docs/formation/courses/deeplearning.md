# Deep learning courses - the list

This is a comprehensive list of courses to learn deep learning, from theory to implementation. Courses are sorted following their medium, for example MOOC, online videos, readings, books, etc...

## Get to Intermediate

- **[Deep learning by 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)**:  
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
4 introduction videos of neural network by the excellent 3Blue1Brown. The entire playlist can be binged-watched in an hour for building an intuition and understanding of deep learning.


- **[Pytorch 60 mn blitz](https://pytorch.org/tutorials/beginner/deep_learning_60min_blitz.html)**:  
![code](https://img.shields.io/badge/content-written-lightgray.svg "Written")
![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")
![code](https://img.shields.io/badge/theory-intermediate-yellow.svg?logo=google-keep "Intermediate")  
Much more practical than the other courses, this quick tutorial expects you to have a broad understanding of both python/numpy and neural network. After one hour, you should be able to develop classical to custom projects in deep learning.

- **[Kaggle learn](https://www.kaggle.com/learn/intro-to-deep-learning)**:  
![content](https://img.shields.io/badge/content-interactive-orange.svg "Interactive")
![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")  
Short course covering the definition of a neural network, up to overfitting and dropout. A very code driven approach, with interactive exercises after each lesson in the form of a in-site notebook.

## Get to Advanced

- **[DeepLeaning.ai](https://www.coursera.org/specializations/deep-learning)**:   
![code](https://img.shields.io/badge/content-MOOC-green.svg?logo=coursera "MOOC")
![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")  
Recognized as the reference course on deep learning. Goes from theory, to implementation using librairies, and through everything in between. The MOOC format can be followed for an expected 2h work a week, but videos are freely accessible on [youtube](https://www.youtube.com/watch?v=CS4cs9xVecg&list=PLkDaE6sCZn6Ec-XTbcX1uRg2_u4xOEky0).


- **[CS231n Stanford](https://www.youtube.com/watch?v=NfnWJUyUJYU&list=PLkt2uSq6rBVctENoVBg1TpCC7OQi31AlC)**:  
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")
![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")
![code](https://img.shields.io/badge/theory-intermediate-yellow.svg?logo=google-keep "Intermediate")  
Stanford lectures of *Convolutional Neural Networks for Visual Recognition*. Videos are theoretical, but assignments are available through their [website](http://cs231n.stanford.edu/). Each video is 1h15 long, but worth your while. This is the 2016 Winter session by Andrej Karpathy.
