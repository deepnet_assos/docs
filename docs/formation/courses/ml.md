## Welcome to Deepnet Courses

### Here you are !

Welcome to Deepnet Courses, If you want to learn Machine Learning, that is the place to be! Here, you can find differents courses about Machine Learning. Made by differents person so you can have differents viewing angle of what is Machine Learing.

<br>

#### Machine Learning - Andrew Ng. (Coursera)
![code](https://img.shields.io/badge/content-MOOK-blue.svg?logo=youtube "Video")  ![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate") <br>
This course is one of the most famous on the Internet due to the date of realease first and then because we have to say that it is one of the most detailled formation available for free on Machine Learning. This course us really complete because it mixes Videos, Texts and Exercices to apply what you learn. This is a strong foundation if you want later to build on Machine Learning Algorithm. <br>
**[Get started with Andrew !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**

**[Check the weeks !](/formation/courses/ml_weeks/week1/)**

<br>

#### Introduction au Machine learning - Guillaume de Saint-Cirgue. (Youtube)
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")     ![code](https://img.shields.io/badge/code-Beginners-green.svg?logo=python "Beginners") <br>
This courses are availables for free on Youtube, the courses are in french so it is not for evrybody but all the aspects that are developed in this course are really well described. The author have good teaching skills so he can make you learn with ease. 
I would recomend this course to evrybody who wants to learn more about Machine Learning. <br>
**[See Machine Learnia !](https://www.youtube.com/c/MachineLearnia)**

<br>

#### Apprenez a programmer en Python - Vincent Le Goff (OpenClassrooms)
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")     ![code](https://img.shields.io/badge/code-Beginners-green.svg?logo=python "Beginners") <br>
You don't know anything about programming and want to learn a clear and intuitive language ? This introdu to Python is made for you ! Later in this class, you will learn how to declare your firsts variables and then code your first algorithm. At the end, you will be able to manage differents library and many more !<br>
**[Learn how to code in Python !](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python)**

<br>

#### Python - Colliin Morris (Kaggle)
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  ![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate") <br>
Learn the basics of the most important language for Data-Science with Colin who     is a data scientist and educator with a background in computational linguistics.<br>
**[Basics of Python !](https://www.kaggle.com/learn/python)**
