# Presentation of Andrew's weeks courses

## Week 1 : Welcome


![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")


Welcome to Machine Learning! This week, we introduce the core idea of teaching a computer to learn concepts using data—without being explicitly programmed.

We are going to start by covering linear regression with one variable. Linear regression predicts a real-valued output based on an input value. We discuss the application of linear regression to housing price prediction, present the notion of a cost function, and introduce the gradient descent method for learning.

#### **- To Begin with :**

##### <ins> Welcome to Machine Learning (1min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, _the Great_ Andrew Ng present us the basics of Machine Learning and explain us what we're going to learn during his next courses. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**

##### <ins> Welcome (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Machine learning is the science of getting computers to act without being explicitly programmed. Many researchers also think it is the best way to make progress towards human-level AI. Finally, Andrew Ng will you about some of Silicon Valley's best practices in innovation as it pertains to machine learning and AI. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**



#### **- Introduction :**

##### <ins> What is Machine Learning ? (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng define us what Machine Learning, he uses the definition of Arthur Samuel & Tom Mitchell and explain it throug the end. He dinstinguishes differents categories in Machine Learnin algorithm, the **Supervised Learning**, **Unsupervised Learning** and the **Reinforcement Learning**. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**

##### <ins> Supervised Learning (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, Andrew Ng present us the differences between Supervised Learning Algrorithm and Unsupervised Learning Algrorithm. He put a finger on Supervised Learning Algorithm with differents exemples. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**

##### <ins> Unsupervised Learning (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Unsupervised learning allows us to approach problems with little or no idea what our results should look like. We can derive structure from data where we don't necessarily know the effect of the variables. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**




#### **- Model and Cost Function :**

##### <ins> Model Representation (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To describe the supervised learning problem slightly more formally, our goal is, given a training set, to learn a function h : X → Y so that h(x) is a “good” predictor for the corresponding value of y. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

##### <ins> Cost Function (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can measure the accuracy of our hypothesis function by using a **cost function**. This takes an average difference of all the results of the hypothesis with inputs from x's and the actual output y's. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/rkTp3/cost-function)**

##### <ins> Cost Function - Intuition I (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Learn how to fit the data ! The best possible line will be such so that the average squared vertical distances of the scattered points from the line will be the least.  

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/N09c6/cost-function-intuition-i)**

##### <ins> Cost Function - Intuition II (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Introduction to contour plot ; this is a graph that contains many contour lines. A contour line of a two variable function has a constant value at all points of the same line. An example of such a graph is the one to the right below. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/nwpe2/cost-function-intuition-ii)**




#### **- Linear Algebra :**

##### <ins> Matrices and Vectors (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Let's get started with our linear algebra review. In this video I want to tell you what are matrices and what are vectors. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/38jIT/matrices-and-vectors)**

##### <ins> Addition and Scalar Multiplication (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video we'll talk about matrix addition and subtraction, as well as how to multiply a matrix by a number, also called Scalar Multiplication. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/R4hiJ/addition-and-scalar-multiplication)**

##### <ins> Matrix Vector Multiplication (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Here, we will be talking about how to multiply together two matrices. We'll start with a special case of that, of matrix vector multiplication - multiplying a matrix together with a vector. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/aQDta/matrix-vector-multiplication)**

##### <ins> Matrix Matrix Multiplication (13min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video we'll talk about matrix-matrix multiplication, or how to multiply two matrices together. When we talk about the method in linear regression for how to solve for the parameters theta 0 and theta 1 all in one shot, without needing an iterative algorithm like gradient descent. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/dpF1j/matrix-matrix-multiplication)**

##### <ins> Matrix  Multiplication Properties (9min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Matrix multiplication is really useful, since you can pack a lot of computation into just one matrix multiplication operation. But you should be careful of how you use them. In this video, you will learn about few properties of matrix multiplication. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/W1LNU/matrix-multiplication-properties)**

##### <ins> Inverse and Transpose (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, we are going to talk about a couple of special matrix operations, called the matrix inverse and the matrix transpose operation. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/FuSWY/inverse-and-transpose)**


## Week 2 : Linear Regression with Multiple Variables

![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")


Welcome to week 2 ! I hope everyone has been enjoying the course and learning a lot! This week we’re covering linear regression with multiple variables. we’ll show how linear regression can be extended to accommodate multiple input features. We also discuss best practices for implementing linear regression.

#### **- Multivariate Linear Regression :**

##### <ins> Multiples Features (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Linear regression with multiple variables is also known as "multivariate linear regression".
We now introduce notation for equations where we can have any number of input variables. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**

#### **- Gradient Descent :**

##### <ins> Gradient Descent for Multiple Variables (5min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng explain us how to properly initialize a Gradient Descent Algorithm with Multiples varialbes. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

##### <ins> Gradient Descent in Practice I - Features Scaling (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can speed up gradient descent by having each of our input values in roughly the same range. This is because θ will descend quickly on small ranges and slowly on large ranges, and so will oscillate inefficiently down to the optimum when the variables are very uneven. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

##### <ins> Gradient Descent in Practice II - Learning Rate (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To Debug gradient descent, make a plot with number of iterations on the x-axis. Now plot the cost function, J(θ) over the number of iterations of gradient descent. If J(θ) ever increases, then you probably need to decrease α. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

##### <ins> Features and Polynomial Regression (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can improve our features and the form of our hypothesis function in a couple different ways.
We can combine multiple features into one. For example, we can combine x1 and x2 into a new features x3 by taking x3 = x1 * x2. Our hypothesis function need not be linear if that does not fit the data well.
We can change the behavior or curve of our hypothesis function by making it a quadratic, cubic or square root function (or any other form).

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**


#### **- Computing Parameters Analytically :**

##### <ins> Normal Equation (16min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng define us what Machine Learning, he uses the definition of Arthur Samuel & Tom Mitchell and explain it throug the end. He dinstinguishes differents categories in Machine Learnin algorithm, the **Supervised Learning**, **Unsupervised Learning** and the **Reinforcement Learning**. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**

##### <ins> Normal Equation Noninvertibility (5min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, Andrew Ng present us the differences between Supervised Learning Algrorithm and Unsupervised Learning Algrorithm. He put a finger on Supervised Learning Algorithm with differents exemples. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**


## Week 3 : Logistic Regression

![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")


Welcome to week 3! This week, we’ll be covering logistic regression. Logistic regression is a method for classifying data into discrete outcomes. For example, we might use logistic regression to classify an email as spam or not spam. In this module, we introduce the notion of classification, the cost function for logistic regression, and the application of logistic regression to multi-class classification.

#### **- Classification and Representation :**

##### <ins> Classification (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To attempt classification, one method is to use linear regression and map all predictions greater than 0.5 as a 1 and all less than 0.5 as a 0. However, this method doesn't work well because classification is not actually a linear function. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**


##### <ins> Hypothesis Representation (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We could approach the classification problem ignoring the fact that y is discrete-valued, and use our old linear regression algorithm to try to predict y given x. However, it is easy to construct examples where this method performs very poorly. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**


##### <ins> Decision Boundary (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
The decision boundary is the line that separates the area where y = 0 and where y = 1. It is created by our hypothesis function. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**


#### **- Logistic Regression Model :**

##### <ins> Cost Function (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We cannot use the same cost function that we use for linear regression because the Logistic Function will cause the output to be wavy, causing many local optima. In other words, it will not be a convex function.

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**


##### <ins> Simplified Cost Function and Gradient Descent (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Notice that this algorithm is identical to the one we used in linear regression. We still have to simultaneously update all values in theta. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**


##### <ins> Advanced Optimization (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
"Conjugate gradient", "BFGS", and "L-BFGS" are more sophisticated, faster ways to optimize θ that can be used instead of gradient descent. We suggest that you should not write these more sophisticated algorithms yourself (unless you are an expert in numerical computing) but use the libraries instead, as they're already tested and highly optimized. Octave provides them. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**


#### **- Multiclass Classification :**

##### <ins> Multiclass Classification: One-vs-all (6min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Now we will approach the classification of data when we have more than two categories. Instead of y = {0,1} we will expand our definition so that y = {0,1...n}. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**


#### **- Solving the Problem of Overfitting :**

##### <ins> The Problem of Overfitting (9min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
This terminology is applied to both linear and logistic regression. There are two main options to address the issue of overfitting: 1. Reduce the number of features, 2. Regularization. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

##### <ins> Cost Function (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
If we have overfitting from our hypothesis function, we can reduce the weight that some of the terms in our function carry by increasing their cost. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

##### <ins> Regularized Linear Regression (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can apply regularization to both linear regression and logistic regression. That's what we will detail later on. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**


##### <ins> Regularized Logistic Regression (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can regularize logistic regression in a similar way that we regularize linear regression. As a result, we can avoid overfitting. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**