# Week 1


![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")

## Welcome :

Welcome to Machine Learning! This week, we introduce the core idea of teaching a computer to learn concepts using data—without being explicitly programmed.

We are going to start by covering linear regression with one variable. Linear regression predicts a real-valued output based on an input value. We discuss the application of linear regression to housing price prediction, present the notion of a cost function, and introduce the gradient descent method for learning.


### Introduction :

#### <ins> Welcome to Machine Learning (1min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, _the Great_ Andrew Ng present us the basics of Machine Learning and explain us what we're going to learn during his next courses. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**

#### <ins> Welcome (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Machine learning is the science of getting computers to act without being explicitly programmed. Many researchers also think it is the best way to make progress towards human-level AI. Finally, Andrew Ng will you about some of Silicon Valley's best practices in innovation as it pertains to machine learning and AI. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**




### **Introduction :**

#### <ins> What is Machine Learning ? (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng define us what Machine Learning, he uses the definition of Arthur Samuel & Tom Mitchell and explain it throug the end. He dinstinguishes differents categories in Machine Learnin algorithm, the **Supervised Learning**, **Unsupervised Learning** and the **Reinforcement Learning**. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**

#### <ins> Supervised Learning (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, Andrew Ng present us the differences between Supervised Learning Algrorithm and Unsupervised Learning Algrorithm. He put a finger on Supervised Learning Algorithm with differents exemples. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**

#### <ins> Unsupervised Learning (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Unsupervised learning allows us to approach problems with little or no idea what our results should look like. We can derive structure from data where we don't necessarily know the effect of the variables. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**




### **Model and Cost Function :**

#### <ins> Model Representation (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To describe the supervised learning problem slightly more formally, our goal is, given a training set, to learn a function h : X → Y so that h(x) is a “good” predictor for the corresponding value of y. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

### Cost Function :

#### <ins> Cost Function (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can measure the accuracy of our hypothesis function by using a **cost function**. This takes an average difference of all the results of the hypothesis with inputs from x's and the actual output y's. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/rkTp3/cost-function)**

#### <ins> Cost Function - Intuition I (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Learn how to fit the data ! The best possible line will be such so that the average squared vertical distances of the scattered points from the line will be the least.  

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/N09c6/cost-function-intuition-i)**

#### <ins> Cost Function - Intuition II (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Introduction to contour plot ; this is a graph that contains many contour lines. A contour line of a two variable function has a constant value at all points of the same line. An example of such a graph is the one to the right below. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/nwpe2/cost-function-intuition-ii)**




### **Linear Algebra :**

#### <ins> Matrices and Vectors (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Let's get started with our linear algebra review. In this video I want to tell you what are matrices and what are vectors. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/38jIT/matrices-and-vectors)**

#### <ins> Addition and Scalar Multiplication (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video we'll talk about matrix addition and subtraction, as well as how to multiply a matrix by a number, also called Scalar Multiplication. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/R4hiJ/addition-and-scalar-multiplication)**

#### <ins> Matrix Vector Multiplication (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Here, we will be talking about how to multiply together two matrices. We'll start with a special case of that, of matrix vector multiplication - multiplying a matrix together with a vector. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/aQDta/matrix-vector-multiplication)**

#### <ins> Matrix Matrix Multiplication (13min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video we'll talk about matrix-matrix multiplication, or how to multiply two matrices together. When we talk about the method in linear regression for how to solve for the parameters theta 0 and theta 1 all in one shot, without needing an iterative algorithm like gradient descent. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/dpF1j/matrix-matrix-multiplication)**

#### <ins> Matrix  Multiplication Properties (9min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Matrix multiplication is really useful, since you can pack a lot of computation into just one matrix multiplication operation. But you should be careful of how you use them. In this video, you will learn about few properties of matrix multiplication. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/W1LNU/matrix-multiplication-properties)**

#### <ins> Inverse and Transpose (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, we are going to talk about a couple of special matrix operations, called the matrix inverse and the matrix transpose operation. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/FuSWY/inverse-and-transpose)**


