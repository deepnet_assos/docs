# Week 2

![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")

## Linear Regression with Multiple Variables :

Welcome to week 2 ! I hope everyone has been enjoying the course and learning a lot! This week we’re covering linear regression with multiple variables. we’ll show how linear regression can be extended to accommodate multiple input features. We also discuss best practices for implementing linear regression.

### Multivariate Linear Regression :

#### <ins> Multiples Features (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Linear regression with multiple variables is also known as "multivariate linear regression".
We now introduce notation for equations where we can have any number of input variables. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**

### Gradient Descent :

#### <ins> Gradient Descent for Multiple Variables (5min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng explain us how to properly initialize a Gradient Descent Algorithm with Multiples varialbes. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

#### <ins> Gradient Descent in Practice I - Features Scaling (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can speed up gradient descent by having each of our input values in roughly the same range. This is because θ will descend quickly on small ranges and slowly on large ranges, and so will oscillate inefficiently down to the optimum when the variables are very uneven. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

#### <ins> Gradient Descent in Practice II - Learning Rate (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To Debug gradient descent, make a plot with number of iterations on the x-axis. Now plot the cost function, J(θ) over the number of iterations of gradient descent. If J(θ) ever increases, then you probably need to decrease α. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**

#### <ins> Features and Polynomial Regression (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can improve our features and the form of our hypothesis function in a couple different ways.
We can combine multiple features into one. For example, we can combine x1 and x2 into a new features x3 by taking x3 = x1 * x2. Our hypothesis function need not be linear if that does not fit the data well.
We can change the behavior or curve of our hypothesis function by making it a quadratic, cubic or square root function (or any other form).

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**




### **Computing Parameters Analytically :**

#### <ins> Normal Equation (16min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this Episode, Andrew Ng define us what Machine Learning, he uses the definition of Arthur Samuel & Tom Mitchell and explain it throug the end. He dinstinguishes differents categories in Machine Learnin algorithm, the **Supervised Learning**, **Unsupervised Learning** and the **Reinforcement Learning**. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**

#### <ins> Normal Equation Noninvertibility (5min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this video, Andrew Ng present us the differences between Supervised Learning Algrorithm and Unsupervised Learning Algrorithm. He put a finger on Supervised Learning Algorithm with differents exemples. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**

