# Week 3

![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")

## Logistic Regression :

Welcome to week 3! This week, we’ll be covering logistic regression. Logistic regression is a method for classifying data into discrete outcomes. For example, we might use logistic regression to classify an email as spam or not spam. In this module, we introduce the notion of classification, the cost function for logistic regression, and the application of logistic regression to multi-class classification.

### Classification and Representation :

#### <ins> Classification (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
To attempt classification, one method is to use linear regression and map all predictions greater than 0.5 as a 1 and all less than 0.5 as a 0. However, this method doesn't work well because classification is not actually a linear function. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-to-machine-learning-zcAuT)**


#### <ins> Hypothesis Representation (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We could approach the classification problem ignoring the fact that y is discrete-valued, and use our old linear regression algorithm to try to predict y given x. However, it is easy to construct examples where this method performs very poorly. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**


#### <ins> Decision Boundary (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
The decision boundary is the line that separates the area where y = 0 and where y = 1. It is created by our hypothesis function. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/welcome-RKFpn)**


### **Logistic Regression Model :**

#### <ins> Cost Function (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We cannot use the same cost function that we use for linear regression because the Logistic Function will cause the output to be wavy, causing many local optima. In other words, it will not be a convex function.

**[See this course !](https://www.coursera.org/lecture/machine-learning/what-is-machine-learning-Ujm7v)**


#### <ins> Simplified Cost Function and Gradient Descent (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Notice that this algorithm is identical to the one we used in linear regression. We still have to simultaneously update all values in theta. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**


#### <ins> Advanced Optimization (14min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
"Conjugate gradient", "BFGS", and "L-BFGS" are more sophisticated, faster ways to optimize θ that can be used instead of gradient descent. We suggest that you should not write these more sophisticated algorithms yourself (unless you are an expert in numerical computing) but use the libraries instead, as they're already tested and highly optimized. Octave provides them. 

**[See this course !](https://www.coursera.org/lecture/machine-learning/supervised-learning-1VkCb)**


### **Multiclass Classification :**

#### <ins> Multiclass Classification: One-vs-all (6min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Now we will approach the classification of data when we have more than two categories. Instead of y = {0,1} we will expand our definition so that y = {0,1...n}. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**


### **Solving the Problem of Overfitting :**

#### <ins> The Problem of Overfitting (9min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
This terminology is applied to both linear and logistic regression. There are two main options to address the issue of overfitting: 1. Reduce the number of features, 2. Regularization. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

#### <ins> Cost Function (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
If we have overfitting from our hypothesis function, we can reduce the weight that some of the terms in our function carry by increasing their cost. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**

#### <ins> Regularized Linear Regression (10min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can apply regularization to both linear regression and logistic regression. That's what we will detail later on. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**


#### <ins> Regularized Logistic Regression (8min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
We can regularize logistic regression in a similar way that we regularize linear regression. As a result, we can avoid overfitting. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/db3jS/model-representation)**