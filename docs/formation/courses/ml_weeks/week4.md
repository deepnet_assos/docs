# Week 4

![code](https://img.shields.io/badge/code-intermediate-yellow.svg?logo=python "Intermediate")

## Neural Networks: Representation :

Welcome to week 3! This week, we’ll be covering logistic regression. Logistic regression is a method for classifying data into discrete outcomes. For example, we might use logistic regression to classify an email as spam or not spam. In this module, we introduce the notion of classification, the cost function for logistic regression, and the application of logistic regression to multi-class classification.

### Neural Networks :

#### <ins> Model Representation I (12min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
Let's examine how we will represent a hypothesis function using neural networks. At a very simple level, neurons are basically computational units that take inputs (dendrites) as electrical inputs (called "spikes") that are channeled to outputs (axons). 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/ka3jK/model-representation-i  )**


#### <ins> Hypothesis Representation II (11min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
In this section we'll do a vectorized implementation of the previous functions by defining a new variable. 

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/Hw3VK/model-representation-ii)**

### Applications :

#### <ins> Examples and Intuitions I (7min) </ins>
![code](https://img.shields.io/badge/content-video-blue.svg?logo=youtube "Video")  
A simple example of applying neural networks is by predicting x1 AND x2 which is the logical 'and' operator and is only true if both x1 and x2 are 1.

**[See this course !](https://www.coursera.org/learn/machine-learning/lecture/rBZmG/examples-and-intuitions-i)**

