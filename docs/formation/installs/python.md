Python3 installation using virtualenv - Ubuntu
======================================

## 1. Installation
We expect the user to be using on Ubuntu 20.04, though most of the command should work on previous versions. Most of the commands are to be run in a terminal, as indicated by the symbol `$` at the beginning of a line. Let's start by updating our packages:

```bash
$ sudo apt update
$ sudo apt upgrade
```

We then install python3, and [pip](https://en.wikipedia.org/wiki/Pip_(package_manager)), a python package manager, to install libraries such as numpy or tensorflow:

```bash
$ sudo apt install python3.8-dev python3-pip
```

We can now install virtualenv:

```bash
$ sudo -H pip3 install virtualenv
```

## 2. Usage

Let's create a virtual environment named ".venv". It's a good practice for its name to start with a dot, making the folder hidden in order to avoid modifying it manualy. You can use the following command in your shell:
```bash
$ python3 -m virtualenv .venv
```

To start working in the environment, use:
```bash
$ source .venv/bin/activate
```
and ```deactivate``` to exit. When activated, the name of the environment is written before the symbol `$`:
```bash
user@computer~$ source .venv/bin/activate
(.venv) user@computer~$
(.venv) user@computer~$ deactivate
user@computer~$
```
